/*
 * xfdf.vala
 *
 * Copyright (C) 2023  Daniel Espinosa <esodan@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *      Daniel Espinosa <esodan@gmail.com>
 */

public class Gxfdf.Xfdf : GXml.Element
{
  /**
   * Provides the PDF file source's name
   */
  public F f { get; set; }
  /**
   * Provides identification of original PDF and modified one
   */
  public Ids ids { get; set; }
  /**
   * Provides access to an array of {@link Gxfdf.Field}
   * defined in the document
   */
  public Fields fields { get; set; }
  /**
   * Provides access to an array of {@link Gxfdf.Annot}
   * defined in the document
   */
  public Annots annotations { get; set; }
  construct {
    try {
      initialize_with_namespace ("http://ns.adobe.com/xfdf/", "xmlns","xfdf");
      set_attribute_ns (null, "xml", "preserve");
    } catch (GLib.Error e) {
      warning ("Error initializing xfdf element: %s", e.message);
    }
  }
}
